package sda.photoclientdatabase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhotoclientdatabaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(PhotoclientdatabaseApplication.class, args);
    }
}
